"""Contains functions for the `Info` service."""

from grpc import RpcError
from image import DrawImage
from tabulate import tabulate

from proto import (
    info_pb2_grpc,
    info_pb2,
    search_pb2_grpc,
    result_type_pb2,
    search_pb2
)


def print_info_result(result: info_pb2.InfoResponse, size: tuple[int, int]) -> None:
    print(f"Showing results for: {result.id}")
    DrawImage.from_url(
        url=result.image,
        size=size
    ).draw_image()
    print()
    print(tabulate(
        [[result.fullTitle, result.title, result.type]],
        headers=["Full Title", "Title", "Type"],
        tablefmt="fancy_grid",
        maxcolwidths=[30, 24, 10],
    ))
    print(tabulate(
        [
            ["ID", result.id],
            ["Release Date", result.releaseDate],
            ["Plot", result.plot],
        ],
        tablefmt="fancy_grid",
        maxcolwidths=[12, 52],
    ))


def grpc_find_info(
    info_service: info_pb2_grpc.InfoStub,
    search_service: search_pb2_grpc.SearchStub,
    image_size: tuple[int, int]
) -> None:
    try:
        search_response = search_service.Search(
            search_pb2.SearchRequest(
                expression=input("Search: "),
                resultType=result_type_pb2.RESULT_TYPE_TITLE,
            )
        )

        if not search_response.results:
            raise RuntimeError("No results found")

        best_result = search_response.results[0]

        print_info_result(
            info_service.Info(
                info_pb2.InfoRequest(
                    id=best_result.id
                )
            ),
            size=image_size
        )
    except (RpcError, RuntimeError) as e:
        print(f"Something went wrong: {e}")


def grpc_info(service: info_pb2_grpc.InfoStub, image_size: tuple[int, int]) -> None:
    try:
        print_info_result(
            service.Info(
                info_pb2.InfoRequest(
                    id=input("Enter a movie/series id: ")
                )
            ),
            size=image_size
        )
    except (RpcError) as e:
        print(f"Something went wrong: {e}")
