# Backend
Listens to GRPC requests and turns them into HTTP requests
to the IMDB api.

## Getting Started
1. Copy the `.env` file and fill in the details (‼️ Do not commit this file ‼️)
2. Run the [`backend`]:
   ```sh
   # Using default arguments
   cargo run

   # Passing custom arguments
   cargo run -- --help
   ```
3. Get coffee ☕ and wait till compilation is done, this will take a while
