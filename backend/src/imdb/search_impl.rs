use std::collections::HashMap;

use reqwest;
use tonic::{async_trait, Request, Response, Result};

use super::proto::search_server::Search;
use super::proto::{ResultType, SearchRequest, SearchResponse};
use super::{err_to_status, reqwest_err_to_status, API_BASE_PATH};

pub struct SearchImpl {
    api_key: String,
}

impl SearchImpl {
    pub fn new(api_key: String) -> Self {
        Self { api_key }
    }

    /// Generalized search method taking care of the boiler-plate code for
    /// a request.
    /// The `search_type` represents the element after `API` in
    /// <https://imdb-api.com/API#accordion1>
    async fn generic_search(
        &self,
        search_type: &str,
        search_request: Request<SearchRequest>,
    ) -> Result<Response<SearchResponse>> {
        Ok(Response::new(
            reqwest::get(format!(
                "{API_BASE_PATH}/{lang}/API/{search_type}/{key}/{expr}",
                lang = search_request.get_ref().lang.clone().unwrap_or("en".into()),
                key = &self.api_key,
                expr = search_request.get_ref().expression,
            ))
            .await
            .map_err(reqwest_err_to_status)?
            .json::<SearchResponse>()
            .await
            .map_err(err_to_status)?,
        ))
    }
}

#[async_trait]
impl Search for SearchImpl {
    async fn search(
        &self,
        search_request: Request<SearchRequest>,
    ) -> Result<Response<SearchResponse>> {
        let result_to_route_map = HashMap::from([
            (ResultType::Title, "SearchTitle"),
            (ResultType::Movie, "SearchMovie"),
            (ResultType::Series, "SearchSeries"),
            (ResultType::Name, "SearchName"),
            (ResultType::Episode, "SearchEpisode"),
            (ResultType::Company, "SearchCompany"),
            (ResultType::Keyword, "SearchKeyword"),
            (ResultType::All, "SearchAll"),
        ]);
        // match search_request.get_ref().resultType {}
        self.generic_search(
            &result_to_route_map
                .get(
                    &search_request.get_ref().result_type(), // .unwrap_or(ResultType::All),
                )
                .ok_or(tonic::Status::not_found("Invalid ResultType"))?,
            search_request,
        )
        .await
    }
}
