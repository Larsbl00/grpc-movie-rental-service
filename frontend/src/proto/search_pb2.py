# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: search.proto
"""Generated protocol buffer code."""
from google.protobuf.internal import builder as _builder
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import result_type_pb2 as result__type__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x0csearch.proto\x12\x04imdb\x1a\x11result_type.proto\"a\n\x0cSearchResult\x12\n\n\x02id\x18\x01 \x01(\t\x12\x12\n\nresultType\x18\x02 \x01(\t\x12\r\n\x05image\x18\x03 \x01(\t\x12\r\n\x05title\x18\x04 \x01(\t\x12\x13\n\x0b\x64\x65scription\x18\x05 \x01(\t\"y\n\rSearchRequest\x12\x12\n\nexpression\x18\x01 \x01(\t\x12)\n\nresultType\x18\x02 \x01(\x0e\x32\x10.imdb.ResultTypeH\x00\x88\x01\x01\x12\x11\n\x04lang\x18\x03 \x01(\tH\x01\x88\x01\x01\x42\r\n\x0b_resultTypeB\x07\n\x05_lang\"s\n\x0eSearchResponse\x12#\n\x07results\x18\x01 \x03(\x0b\x32\x12.imdb.SearchResult\x12\x12\n\nsearchType\x18\x02 \x01(\t\x12\x12\n\nexpression\x18\x03 \x01(\t\x12\x14\n\x0c\x65rrorMessage\x18\x04 \x01(\t2=\n\x06Search\x12\x33\n\x06Search\x12\x13.imdb.SearchRequest\x1a\x14.imdb.SearchResponseb\x06proto3')

_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, globals())
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'search_pb2', globals())
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _SEARCHRESULT._serialized_start=41
  _SEARCHRESULT._serialized_end=138
  _SEARCHREQUEST._serialized_start=140
  _SEARCHREQUEST._serialized_end=261
  _SEARCHRESPONSE._serialized_start=263
  _SEARCHRESPONSE._serialized_end=378
  _SEARCH._serialized_start=380
  _SEARCH._serialized_end=441
# @@protoc_insertion_point(module_scope)
