"""Generated proto interfaces.

NOTE: Do **NOT** alter the content manually, generate it instead.
"""

import os
import sys

# Import magic, see this for details:
# https://stackoverflow.com/questions/53934591/when-i-try-to-generate-files-for-protobuf-i-get-error-modulenotfounderror#answer-55258233
sys.path.insert(0, os.path.abspath(os.path.dirname(__file__)))
