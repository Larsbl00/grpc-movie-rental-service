# GRPC IMDB connection Service
A [gRPC] interface for the IMDB API in rust.
Alongside a python CLI frontend.
This project is mainly used to become more familiar with [gRPC].
Therefore, no official support will be provided.

![Example search result](./img/preview.png)

## Useful Links:
- IMDB API docs: <https://imdb-api.com/api>

## Getting Started
After cloning the repository, the project has two main services 
[`frontend`] and [`backend`].
1. [`Backend`]
   1. Change directory to `./backend`
   2. Copy the `.env` file and fill in the details (‼️ Do not commit this file ‼️)
   3. Run the [`backend`]:
      ```sh
      # Using default arguments
      cargo run
  
      # Passing custom arguments
      cargo run -- --help
      ```
    5. Get coffee ☕ and wait till compilation is done, this will take a while
 2. [`Frontend`]
    1. Change directory to `./frontend`
    2. Follow the information in the [README](./frontend/README.md)
    3. Play around with the available commands

## General architecture
This project consists of the frontend and backend. 
The frontend sends requests to the backend,
which in turn sends the data to the IMDB api.
This should look a bit like this (implementations may by slightly
different)

```mermaid
sequenceDiagram
    participant Backend
    participant Frontend
    participant IMDB API

    Frontend ->> Frontend: Get user input 
    Frontend ->>+ Backend: Translate request
    Backend ->>+ IMDB API: Actual request
    IMDB API -->>- Backend: Response
    Backend -->>- Frontend: Translate response
    Frontend ->> Frontend: Display Content 
```

## Demos


### Youtube
[![Demo video (youtube)](http://img.youtube.com/vi/iSdhcRt2rGU/0.jpg)](https://www.youtube.com/watch?v=iSdhcRt2rGU)

### Local Video
TLDR, backup in case the youtube one breaks

![Demo video (file)](videos/grpc_imdb_demo.mp4)


[`backend`]: ./backend
[`frontend`]: ./frontend
[gRPC]: https://grpc.io/
