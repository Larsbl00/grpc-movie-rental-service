"""Front end used to communicate with the GRPC backend."""

import argparse
from typing import List, NoReturn

import grpc
from tabulate import tabulate

from proto import search_pb2_grpc, result_type_pb2, info_pb2_grpc

from action import Action
from search import grpc_search, SEARCH_RESULT_MAP
from info import grpc_info, grpc_find_info


IMAGE_DISPLAY_SIZE = (48, 32)


def selection_menu(
    actions: List[Action],
) -> bool:
    """Prompt the selection menu.

    Arguments:
        actions(List[Action[search_pb2_grpc.SearchStub]]): List of actions to
            be prompted and executed
        service(search_pb2_grpc.SearchStub): Stub used to communicate
            with the search service
        info_service(info_pb2_grpc.InfoStub): Stub used to communicate
            with the info service

    Returns:
        bool: `True` to keep running, `False` to terminate
    """
    print()
    print(tabulate(
        [
            [i, action.name, action.description]
            for (i, action) in enumerate(actions)
        ],
        headers=["Key", "Action", "Description"],
        tablefmt="fancy_grid",
        maxcolwidths=[2, 18, 44],
    ))

    try:
        actions[int(input("Select action: "))].action()
    except (ValueError, IndexError) as e:
        print(f"Error getting input, try again. ({e})")

    return True


def main() -> None:
    """Program entry point."""
    arg_parser = argparse.ArgumentParser(
        description=__doc__
    )

    arg_parser.add_argument(
        "-a", "--address",
        metavar="[::1]:50051",
        help="Address of the gRPC service",
        default="[::1]:50051",
    )

    arg_parser.add_argument(
        "-l", "--page-limit",
        metavar="5",
        help="Amount of items shown per page",
        default=5,
        type=int
    )

    args = arg_parser.parse_args()

    with grpc.insecure_channel(args.address) as channel:
        search_stub = search_pb2_grpc.SearchStub(channel)
        info_stub = info_pb2_grpc.InfoStub(channel)

        actions: List[Action[search_pb2_grpc.SearchStub]] = [
            Action(
                "Exit",
                lambda: exit(0),
                description="Exits the application"
            ),
            Action(
                "Search",
                lambda: grpc_search(
                    search_stub,
                    args.page_limit,
                    result_type_pb2.RESULT_TYPE_TITLE
                ),
                description="Searches for an item",
            ),
            Action(
                "Search Type",
                lambda: grpc_search(
                    search_stub,
                    args.page_limit,
                    search_type=SEARCH_RESULT_MAP[
                        input(
                            f"What are your looking for? ({', '.join(SEARCH_RESULT_MAP)}): "
                        ).casefold()
                    ]
                ),
                description="Searches for an item,"
                " but allows you to narrow the type of the result",
            ),
            Action(
                "Find Info",
                lambda: grpc_find_info(
                    info_service=info_stub,
                    search_service=search_stub,
                    image_size=IMAGE_DISPLAY_SIZE,
                ),
                description="Get info from the first result found based on the"
                " name",
            ),
            Action(
                "Info",
                lambda: grpc_info(info_stub, IMAGE_DISPLAY_SIZE),
                description="Get info from an item, based on id",
            ),
        ]

        while selection_menu(actions):
            pass


if __name__ == "__main__":
    main()
