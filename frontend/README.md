# Frontend
A very simple front-end service used to communicate to the 
gRPC [backend](../backend).

## Getting Started
To run the app, perform the following steps:
1. Follow the [prerequisites](#prerequisites)
2. Run the main application
   ```sh
   # No arguments
   python3 src/main.py

   # With arguments
   python3 src/main.py --help
   ```

### Prerequisites
Before running the program, please perform the following steps first:
1. Install dependencies
   ```sh
    pip3 install -r requirements.txt
   ```
2. If there are no interfaces in the `src/proto` folder,
   [generate them](#interface-generation)



### Interface generation:
Execute the following command from within this directory
(i.e. `frontend`) to generate source files for the buffer:
```sh
./generate_proto.sh
```

