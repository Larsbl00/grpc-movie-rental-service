use std::error::Error;
use std::env;

use clap::{Parser, ValueHint};
use dotenv;
use tokio;
use tonic::transport::Server;

use imdb::proto::{search_server::SearchServer, info_server::InfoServer};

mod imdb;


#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(
        long,
        short = 'e', 
        value_name = ".env", 
        default_value_t = String::from(".env"), 
        value_hint=ValueHint::FilePath, 
    )]
    env_file: String,

    #[arg(
        long,
        short = 'a',
        value_name = "[::1]:50051",
        default_value_t = String::from("[::1]:50051"),
        value_hint = ValueHint::Url,
    )]
    address: String,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();
    dotenv::from_path(args.env_file).ok();

    let api_key = env::var("IMDB_API_KEY")?;

    Server::builder()
        .add_service(SearchServer::new(
            imdb::SearchImpl::new(
                api_key.clone()
            )
        ))
        .add_service(InfoServer::new(
            imdb::InfoImpl::new(
                api_key.clone()
            )
        ))
        .serve(args.address.parse()?)
        .await?;
    Ok(())
}
