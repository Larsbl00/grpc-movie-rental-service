"""Functionality related to handling actions."""

from dataclasses import dataclass

from typing import Callable


@dataclass
class Action:
    """Object containing relevant information to perform an action."""

    name: str
    action: Callable[[], None]
    description: str = ""
