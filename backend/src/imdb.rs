//! Implements the IMDB GRPC services

use std::error::Error;

use reqwest::StatusCode;
use tonic::Status;

pub mod proto {
    tonic::include_proto!("imdb");
}

mod info_impl;
mod search_impl;

pub use info_impl::*;
pub use search_impl::*;

const API_BASE_PATH: &'static str = "https://imdb-api.com";

fn err_to_status<E>(err: E) -> tonic::Status
where
    E: Error,
{
    Status::unknown(err.to_string())
}

fn reqwest_err_to_status(err: reqwest::Error) -> tonic::Status {
    match err.status() {
        None => Status::ok(err.to_string()),
        Some(code) => match code {
            StatusCode::FORBIDDEN => Status::permission_denied(err.to_string()),
            _ => Status::unknown(err.to_string()),
        },
    }
}
