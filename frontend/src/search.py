"""Contains functions for the `Search` service."""

from tabulate import tabulate
from grpc import RpcError

from proto import search_pb2_grpc, search_pb2, result_type_pb2


SEARCH_RESULT_MAP = {
    "title": result_type_pb2.RESULT_TYPE_TITLE,
    "movie": result_type_pb2.RESULT_TYPE_MOVIE,
    "series": result_type_pb2.RESULT_TYPE_SERIES,
    "name": result_type_pb2.RESULT_TYPE_NAME,
    "episode": result_type_pb2.RESULT_TYPE_EPISODE,
    "company": result_type_pb2.RESULT_TYPE_COMPANY,
    "keyword": result_type_pb2.RESULT_TYPE_KEYWORD,
    "all": result_type_pb2.RESULT_TYPE_ALL,
}


def print_search_result(
    result: search_pb2.SearchResponse,
    page_limit: int,
) -> None:
    print(f"Results for '{result.expression}':")
    print(tabulate(
        [
            [
                e.id, e.resultType, e.title, e.description
            ] for e in result.results[:page_limit]
        ],
        headers=["ID", "Result Type", "Title", "Description"],
        tablefmt="fancy_grid",
        maxcolwidths=[12, 12, 20, 20],
    ))
    if len(result.results) > page_limit:
        print(f"* Only {page_limit} item(s) shown")


def grpc_search(
    service: search_pb2_grpc.SearchStub,
    page_limit: int,
    search_type: result_type_pb2.ResultType,
) -> None:
    try:
        print_search_result(
            service.Search(search_pb2.SearchRequest(
                expression=input("Search: "),
                resultType=search_type,
            )),
            page_limit,
        )
    except (KeyError):
        print("That is not a valid option, please try again")
    except (RpcError) as e:
        print(f"Something went wrong: {e}")
