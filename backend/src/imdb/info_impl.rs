use reqwest;
use tonic::{async_trait, Request, Response, Result};

use super::proto::info_server::Info;
use super::proto::{InfoRequest, InfoResponse};
use super::{err_to_status, reqwest_err_to_status, API_BASE_PATH};

pub struct InfoImpl {
    api_key: String,
}

impl InfoImpl {
    pub fn new(api_key: String) -> Self {
        Self { api_key }
    }
}

#[async_trait]
impl Info for InfoImpl {
    async fn info(&self, info_request: Request<InfoRequest>) -> Result<Response<InfoResponse>> {
        if !info_request.get_ref().id.starts_with("tt") {
            return Err(tonic::Status::invalid_argument("Id is not valid"));
        }

        Ok(Response::new(
            reqwest::get(format!(
                "{API_BASE_PATH}/{lang}/API/Title/{key}/{id}",
                lang = info_request.get_ref().lang.clone().unwrap_or("en".into()),
                key = &self.api_key,
                id = info_request.get_ref().id,
            ))
            .await
            .map_err(reqwest_err_to_status)?
            .json::<InfoResponse>()
            .await
            .map_err(err_to_status)?,
        ))
    }
}
