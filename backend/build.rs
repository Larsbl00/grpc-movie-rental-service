use std::error::Error;

use tonic_build;

fn main() -> Result<(), Box<dyn Error>> {
    tonic_build::configure()
        .build_client(false)
        .type_attribute(
            ".",
            r#"
        #[derive(serde::Serialize, serde::Deserialize)]
        #[serde(rename_all = "camelCase")]"#,
        )
        .protoc_arg("--experimental_allow_proto3_optional")
        .compile(
            &[
                "../proto/info.proto",
                "../proto/result_type.proto",
                "../proto/search.proto",
            ],
            &["../proto/"],
        )?;

    Ok(())
}
